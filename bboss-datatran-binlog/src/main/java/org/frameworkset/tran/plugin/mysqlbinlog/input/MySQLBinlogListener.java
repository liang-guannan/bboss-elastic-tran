package org.frameworkset.tran.plugin.mysqlbinlog.input;
/**
 * Copyright 2023 bboss
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.BinaryLogFileReader;
import com.github.shyiko.mysql.binlog.event.*;
import com.github.shyiko.mysql.binlog.event.deserialization.EventDeserializer;

import java.io.File;
import java.io.IOException;

/**
 * <p>Description: binlog监听器</p>
 * <p></p>
 * <p>Copyright (c) 2023</p>
 * @Date 2023/4/3
 * @author biaoping.yin
 * @version 1.0
 */
public class MySQLBinlogListener {
    public static void main(String[] args){
        MySQLBinlogListener mySQLBinlogListener = new MySQLBinlogListener();
        try {
//            for(int i = 1; i < 114; i ++) {
//
//                if(i < 10){
//                    mySQLBinlogListener.listBinLogFile("F:\\6_environment\\mysql\\binlog.00000"+i );
//                }
//                else if(i < 100){
//                    mySQLBinlogListener.listBinLogFile("F:\\6_environment\\mysql\\binlog.0000"+i );
//                }
//                else{
//                    mySQLBinlogListener.listBinLogFile("F:\\6_environment\\mysql\\binlog.000"+i );
//                }
//            }
            mySQLBinlogListener.listBinLogFile("F:\\6_environment\\mysql\\binlog.000107" );

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void listMasterSlave(){
        BinaryLogClient client = new BinaryLogClient("hostname", 3306, "username", "password");
        EventDeserializer eventDeserializer = new EventDeserializer();
        eventDeserializer.setCompatibilityMode(
                EventDeserializer.CompatibilityMode.DATE_AND_TIME_AS_LONG,
                EventDeserializer.CompatibilityMode.CHAR_AND_BINARY_AS_BYTE_ARRAY
        );
        client.setEventDeserializer(eventDeserializer);
        client.registerEventListener(new BinaryLogClient.EventListener() {

            @Override
            public void onEvent(Event event) {

            }
        });
        try {
            client.connect();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void listBinLogFile(String c) throws IOException {
        File binlogFile = new File(c);
        EventDeserializer eventDeserializer = new EventDeserializer();
        eventDeserializer.setCompatibilityMode(
                EventDeserializer.CompatibilityMode.DATE_AND_TIME_AS_LONG,
                EventDeserializer.CompatibilityMode.CHAR_AND_BINARY_AS_BYTE_ARRAY
        );
        BinaryLogFileReader reader = new BinaryLogFileReader(binlogFile, eventDeserializer);
        try {
            String table = null;
            String database = null;
            for (Event event; (event = reader.readEvent()) != null; ) {
                EventData eventData = event.getData();
                if(eventData instanceof TableMapEventData) {
                    TableMapEventData writeRowsEventData = (TableMapEventData)event.getData();
                     table = writeRowsEventData.getTable();
                     database = writeRowsEventData.getDatabase();
                    System.out.println(event);
                } else if(eventData instanceof WriteRowsEventData) {
                    WriteRowsEventData writeRowsEventData = (WriteRowsEventData)event.getData();
                    writeRowsEventData.getIncludedColumns();
//                    System.out.println(event);
                }
                else if(eventData instanceof UpdateRowsEventData) {
                    UpdateRowsEventData writeRowsEventData = (UpdateRowsEventData)event.getData();
                    writeRowsEventData.getIncludedColumns();
//                    System.out.println(event);
                }
                else if(eventData instanceof DeleteRowsEventData) {
                    DeleteRowsEventData writeRowsEventData = (DeleteRowsEventData)event.getData();
                    writeRowsEventData.getIncludedColumns();
//                    System.out.println(event);
                }
            }
        } finally {
            reader.close();
        }
    }
}
